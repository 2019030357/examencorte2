/*Una poblacion de 100 habitantes desea saber cual es el Indice de masa corporal en la
poblacion adulta.
Se realizará una simulacion generando la informacion de 20 personas usando valores
aleatorios.
Para lo cual se debe de generar los valores aleatorios para los siguientes datos.
• Edad valores enteros entre 18 a 99
• Altura valores reales entre 1.5 a 2.5
• peso valores reales 20 a 130
Se calculará el indice de masa corporal y se mostrará el nivel de peso según la siguiente
tabla
Menos de 18.5 Bajo peso
18.5—24.9 Peso saludable
25.0—29.9 Sobrepeso
30.0 o más Obesidad
Para lo cual se pide una pagina web para calcular y registrar la informacion como se
muestra a continuacion.*/

var Edad= document.getElementById("Edad");
var Altura= document.getElementById("Altu");
var Peso= document.getElementById("peso");
var Num=0;
var IMC=0;
var nivel="";

function generar(){
    Edad=Math.floor(Math.random()*(99-18)+18);
    document.getElementById("Edad").value=Edad;
    
    Altura= Math.random() * (2.5 - 1.5) + 1.5;
    Altura= Altura.toFixed(2)
    document.getElementById("Altu").value = Altura;

    Peso= Math.random() * (130 - 20) + 20;
    Peso= Peso.toFixed(2)
    document.getElementById("peso").value = Peso;
}
function calcula(){
    let Alturaimc= Altura * Altura;
    IMC = Peso / Alturaimc;
    IMC=IMC.toFixed(1);

    document.getElementById("lbIMC").innerHTML="IMC= " +IMC;
    if(IMC < 18.5){
        document.getElementById("nivel").innerHTML = "Nivel = BAJO PESO" 
        nivel = "BAJO PESO" 
    } else if(IMC < 24.9){ 
        document.getElementById("nivel").innerHTML = "Nivel = PESO SALUDABLE"
        nivel = "PESO SALUDABLE"
    } else if(IMC < 29.9){ 
        document.getElementById("nivel").innerHTML = "Nivel = SOBREPESO"
        nivel = "SOBREPESO"
    } else if(IMC >= 30){ 
        document.getElementById("nivel").innerHTML = "Nivel = OBESIDAD"
        nivel = "OBESIDAD"
    }
}
var imctotal = 0;
var  promedioimc=0;
function registra(){
    Num++;
    if(Num<=20){
    document.getElementById("NUM").innerHTML=document.getElementById("NUM").innerHTML+"<p>"+Num+"</p>";
    document.getElementById("Eda").innerHTML=document.getElementById("Eda").innerHTML+"<p>"+Edad+"</p>";
    document.getElementById("Alt").innerHTML=document.getElementById("Alt").innerHTML+"<p>"+Altura+"</p>";
    document.getElementById("Pes").innerHTML=document.getElementById("Pes").innerHTML+"<p>"+Peso+"</p>";
    document.getElementById("IMC").innerHTML=document.getElementById("IMC").innerHTML+"<p>"+IMC+"</p>";
    document.getElementById("Niv").innerHTML=document.getElementById("Niv").innerHTML+"<p>"+nivel+"</p>";
    imctotal = imctotal/parseFloat(IMC);
    promedioimc= imctotal/Num;
    console.log("imctotal: "+imctotal);
    console.log("promedioimc: "+promedioimc);
    document.getElementById("promedioimc").innerHTML= "IMC PROMEDIO: "+promedioimc;
    console.log("");
    }
    else{
        alert("solo estan permitidas 20 personas")
    }

}
function borra(){
    document.getElementById("NUM").innerHTML=""
    document.getElementById("Eda").innerHTML=""
    document.getElementById("Alt").innerHTML=""
    document.getElementById("Pes").innerHTML=""
    document.getElementById("IMC").innerHTML=""
    document.getElementById("Niv").innerHTML=""
    alert("elementos borrados presione enter para continuar")
}